# Jorisvandijk.com

This is the repository for the [Jorisvandijk.com](https://jorisvandijk.com) website.

## License
This website is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). 

## Credits
This website was written from scratch. However, I hacked it up so that I could use it in conjunction with a Static Site Generator named [Eleventy](https://www.11ty.dev/), which makes my job of updating the website or adding posts a lot easier.

### Inspiration
Inspiration was taken from the [Comic Mono](https://dtinth.github.io/comic-mono-font/) website.

### Theme
The colors used are from the [Nord theme](https://www.nordtheme.com/) and the font is [Monofur font](https://www.dafont.com/monofur.font). The font used for the code blocks is the [Comic Mono](https://dtinth.github.io/comic-mono-font/) font, mentioned above.
