<!DOCTYPE html>

<!-- By Joris van Dijk | Jorisvandijk.com-->
<!-- This work is licensed under CC BY-SA 4.0  -->

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Auto mounting a drive in Linux</title>
    <meta name="description" content="Using ftab">
    <meta name="generator" content="Eleventy v1.0.2">
    <link rel="alternate" href="/feed/feed.xml" type="application/atom+xml" title="Jorisvandijk.com">
    <link rel="alternate" href="/feed/feed.json" type="application/json" title="Jorisvandijk.com">
		<link rel="shortcut icon" href="/img/favicon.png">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/everforest.css">
	</head>
	<body>
		<h1 class="header"><a href="/">Jorisvandijk.com</a></h1>
		<h2 class="subheader">Living on Linux</h2>
    <main class="tmpl-post">
      

<p class="date" ><time datetime="2023-05-16">16 May 2023</time></p>
<h1>Auto mounting a drive in Linux</h1>


<p><a href="/img/fstab.png"><img src="/img/fstab_thumb.png" alt="fstab"></a></p>
<p>The goal here is to automatically mount an SSD drive I added internally to my laptop when I boot it up.</p>
<h2>Creating the directory structure</h2>
<p>We will be mounting the drive to <strong>/media/data</strong>. The <strong>/media</strong> folder is where you <em>should</em> be mounting drives on a Linux system. I am on Arch, which does not come with this folder out of the box, so I will have to create it and the <strong>data</strong> folder, which is the name I am giving the drive I am mounting. You can name yours anything you'd like.</p>
<pre class="language-bash"><code class="language-bash"><span class="token builtin class-name">cd</span> /<br><span class="token function">sudo</span> <span class="token function">mkdir</span> media<br><span class="token builtin class-name">cd</span> media<br><span class="token function">sudo</span> <span class="token function">mkdir</span> data</code></pre>
<h2>Getting information on the drive to be mounted</h2>
<p>Next we'll need some information on the drive that we'll be mounting. Most importantly its UUID, which is an id the system uses to recognize this drive.</p>
<p>First well use <em>lsblk</em> to find the right drive.</p>
<pre class="language-bash"><code class="language-bash">lsblk</code></pre>
<p>This will list all drives on your system. Usually it's easy to see which name is associated with your drive by looking at the size of the drives. This is my output:</p>
<pre class="language-bash"><code class="language-bash">NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS<br>sda           <span class="token number">8</span>:0    <span class="token number">1</span>  <span class="token number">57</span>.3G  <span class="token number">0</span> disk <br>├─sda1        <span class="token number">8</span>:1    <span class="token number">1</span>   798M  <span class="token number">0</span> part <br>└─sda2        <span class="token number">8</span>:2    <span class="token number">1</span>    15M  <span class="token number">0</span> part <br>zram0       <span class="token number">254</span>:0    <span class="token number">0</span>     4G  <span class="token number">0</span> disk <span class="token punctuation">[</span>SWAP<span class="token punctuation">]</span><br>nvme1n1     <span class="token number">259</span>:0    <span class="token number">0</span> <span class="token number">465</span>.8G  <span class="token number">0</span> disk <br>├─nvme1n1p1 <span class="token number">259</span>:3    <span class="token number">0</span>   511M  <span class="token number">0</span> part /boot<br>└─nvme1n1p2 <span class="token number">259</span>:4    <span class="token number">0</span> <span class="token number">465</span>.3G  <span class="token number">0</span> part /home<br>                                      /var/cache/pacman/pkg<br>                                      /var/log<br>                                      /.snapshots<br>                                      /<br>nvme0n1     <span class="token number">259</span>:1    <span class="token number">0</span>   <span class="token number">1</span>.9T  <span class="token number">0</span> disk <br>└─nvme0n1p1 <span class="token number">259</span>:2    <span class="token number">0</span>   <span class="token number">1</span>.9T  <span class="token number">0</span> part </code></pre>
<p>The drive I want is the one with 1.9T of storage space and I would like to use the &quot;nvme0n1p1&quot; partition on it. Remember this name, we'll need it in a second.</p>
<pre class="language-bash"><code class="language-bash"><span class="token function">sudo</span> blkid</code></pre>
<p>This will provide you with an output which lists all drives and partitions on your system including their id information and more. Find the drive with the name from the command above. Mine looks like this:</p>
<pre class="language-bash"><code class="language-bash">/dev/nvme1n1p1: <span class="token assign-left variable">LABEL</span><span class="token operator">=</span><span class="token string">"Data"</span> <span class="token assign-left variable">UUID</span><span class="token operator">=</span><span class="token string">"ab8bbd93-ebbf-4827-b08c-6d89efe123ef"</span> <span class="token assign-left variable">UUID_SUB</span><span class="token operator">=</span><span class="token string">"a0133592-e3ce-4879-a41d-cd1b181753b0"</span> <span class="token assign-left variable">BLOCK_SIZE</span><span class="token operator">=</span><span class="token string">"4096"</span> <span class="token assign-left variable">TYPE</span><span class="token operator">=</span><span class="token string">"btrfs"</span> <span class="token assign-left variable">PARTUUID</span><span class="token operator">=</span><span class="token string">"f0a17384-01"</span></code></pre>
<h2>Adding the drive to fstab</h2>
<p>Auto mount magic will be done for us by fstab. Open the fstab file, replacing [your editor] with the editor of your choice. If you're unsure, you can use nano.</p>
<pre class="language-bash"><code class="language-bash"><span class="token function">sudo</span> <span class="token punctuation">[</span>your editor<span class="token punctuation">]</span> /etc/fstab</code></pre>
<p>We will now add our drive to the bottom of the fstab file in the following format:</p>
<pre class="language-bash"><code class="language-bash"><span class="token comment"># [name]</span><br><span class="token assign-left variable">UUID</span><span class="token operator">=</span><span class="token punctuation">[</span>uuid of your drive<span class="token punctuation">]</span>  <span class="token punctuation">[</span>mount point<span class="token punctuation">]</span>  <span class="token punctuation">[</span>file system type<span class="token punctuation">]</span>  <span class="token punctuation">[</span>mount option<span class="token punctuation">]</span>  <span class="token punctuation">[</span>dump<span class="token punctuation">]</span>  <span class="token punctuation">[</span>pass<span class="token punctuation">]</span></code></pre>
<ul>
<li><strong>[name]</strong> is not needed, but makes it easier to identify the drive. You can name it anything you like. The hash symbol in front of it is needed, as this is a comment.</li>
<li><strong>[uuid of your drive]</strong> is the id we got from the <em>blkid</em> command.</li>
<li><strong>[mount point]</strong> is where we want to mount the drive. This is the location we created in the first step, <strong>/media/data</strong> in my case.</li>
<li><strong>[file system type]</strong> is exactly what is says. This is specified as <strong>TYPE</strong> in the <em>blkid</em> command. It is <strong>btrfs</strong> in my case.</li>
<li><strong>[mount option]</strong> I am not getting into right now. Setting it to <strong>defaults</strong> is fine.</li>
<li><strong>[dump]</strong> is meant for stating if this drive should be enabled or disabled when backing up. 0 would be disabling it, 1 enabling it. I will be <strong>disabling</strong> it.</li>
<li><strong>[pass]</strong> controls if fsck should check the device for errors on boot time. Root devices should always be on 1. Other partitions on 2, or 0 to disable checking. I am setting this to <strong>0</strong>, thus disabling it.</li>
</ul>
<p>Putting it all together I will add the following to the fstab file:</p>
<pre class="language-bash"><code class="language-bash"><span class="token comment"># /dev/nvme1n1p1</span><br><span class="token assign-left variable">UUID</span><span class="token operator">=</span>ab8bbd93-ebbf-4827-b08c-6d89efe123ef /media/data btrfs defaults <span class="token number">0</span> <span class="token number">0</span></code></pre>
<p>Save and close the file.</p>
<h2>Enable the changes</h2>
<p>We will first reload the daemon so systemd will use the newly modified version of fstab.</p>
<pre class="language-bash"><code class="language-bash"><span class="token function">sudo</span> systemctl daemon-reload</code></pre>
<p>Next we will mount the new drive manually.</p>
<pre class="language-bash"><code class="language-bash"><span class="token function">sudo</span> <span class="token function">mount</span> <span class="token parameter variable">-a</span></code></pre>
<p>Be sure to check if there are any issues by taking a look at dmsg.</p>
<pre class="language-bash"><code class="language-bash"><span class="token function">sudo</span> <span class="token function">dmesg</span> <span class="token parameter variable">-wH</span></code></pre>
<p>If all went well, the new drive should be mounted. You can check in your file manager or re-run the <em>lsblk</em> command to see that after the name of your drive, there's now also a mount point listed.</p>
<h2>Owning the drive</h2>
<p>Considering this is all done by your system, chances are the file system on your drive is owned by root. This is not very handy, as you'd constantly would have to interact with it as root (through sudo). We do not want this, so we will be handing ownership of the entire <strong>media</strong> directory and everything in it to our user.</p>
<pre class="language-bash"><code class="language-bash"><span class="token function">sudo</span> <span class="token function">chown</span> <span class="token parameter variable">-R</span> <span class="token punctuation">[</span>username<span class="token punctuation">]</span> /media </code></pre>
<p>Where [username] would be your own username, obviously.</p>
<h2>Reboot</h2>
<p>Reboot to see it it all went fine. Remember, if there was a problem your system might not boot. You can edit fstab through a TTY at <strong>/etc/fstab</strong>.</p>
<p>And that's it. Every time you boot, your drive will boot with you!</p>


<ul>
  <li><a href="/index.html">Home</a></li><li>Next: <a href="/posts/044_sorting_movie_folder/">Sorting your movie directory</a></li><li>Previous: <a href="/posts/042_skyrim_round_four/">Skyrim Special Edition with mods on Linux IV</a></li>
</ul>

    </main>
    <footer></footer>
    <!-- Current page: /posts/043_auto_mountin_a_drive_in_Linux/ -->
  </body>
</html>
